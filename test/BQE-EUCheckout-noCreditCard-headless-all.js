const puppeteer = require('puppeteer');
const expect = require('chai').expect;

const {click,getText,getCount,shouldNotExist,waitForText} = require('../lib/helper')

describe('BQE EU EC Order Process - Member Checkout',()=>{
    let browser
    let page
    //Test Hooks:before, beforeEach, after, afterEach
    //before:每個test case執行之前先做的動作(page/browswer)
    before(async function(){
        browser=await puppeteer.launch({
            executablePath:
            "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            headless:true,//有無需要開視窗,false要開,true不開
            //slowMo:100,// slow down by 100ms
            //devtools:false//有無需要開啟開發人員工具
        })
        page=await browser.newPage()
        await page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
        await page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    })
    //after:每個test case執行之後統一要做的動作(page/browswer)
    after(async function(){
        await browser.close()
    })
    // beforeEach(async function(){
    //     //Runs before each test steps(pre-step;像是登入)
    //     await page.goto('http://example.com/')
    // })
    //afterEach(async function(){
        //Runs after each test steps(所有test step執行完的步驟)
    //})
    it('Go to Product Page',async function(){
        await page.setViewport({width:1200,height:1000})
        await page.goto('https://shop.benq.eu/eu-buy/ex3501r.html')
        const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
        await page.click(closeCookie)
        await page.waitForTimeout(5000)//等待5000毫秒
        const buyNow = '#product-addtocart-button'
        await page.click(buyNow)
        await page.waitForTimeout(20000)//等待20000毫秒
    })
    it('Cart Page',async function(){
        const cartPageUrl = await page.url()
        console.log('Cart Page URL:',cartPageUrl)
        expect(cartPageUrl).to.include('checkout/cart')//斷言:此page的url必須包含example.com
        const addItems = '#shopping-cart-table > tbody > tr > td.col.qty > div > div > div > button.increaseQty-cart-item'
        await page.click(addItems)//增加商品
        await page.waitForTimeout(5000)
        const qtyUpdate = '#shopping-cart-table > tbody > tr > td.col.qty > div > div > span.input-text-qty'
        await page.waitForSelector(qtyUpdate)//qty更新
        var qty = await getText(page, qtyUpdate)
        expect(qty).to.be.a('string','2')
        const memberCheckoutButton = '#maincontent > div.columns > div > div.cart-summary > ul > li:nth-child(1) > button'
        await page.waitForSelector(memberCheckoutButton)//Member Checkout
        await page.click(memberCheckoutButton)//Member Checkout
        await page.waitForTimeout(20000)//等待20000毫秒
    })
    it('Login Page',async function(){
        const loginPageUrl = await page.url()
        console.log('Login Page URL:',loginPageUrl)
        expect(loginPageUrl).to.include('https://club.benq.eu/ICDS_EU/Home/BenQAuth?system_id=Magento&function=Login&lang=en-eu')//斷言:此page的url必須包含example.com
        await page.waitForTimeout(1000)//等待1000毫秒
        await page.waitForSelector('body > div.block.log_in > div > div > div.col-sm-7.col-xs-12.log_left > div')
        await page.type('#userName','celinetest123@gmail.com',{delay:100})
        await page.type('#password','test1234',{delay:100})
        await page.click('#login')
        await page.waitForTimeout(30000)//等待30000毫秒
    })
    it('Shipping Page',async function(){
        const shippingPageUrl = await page.url()
        console.log('Shipping Page URL:',shippingPageUrl)
        expect(shippingPageUrl).to.include('/checkout/')//斷言:此page的url必須包含example.com
        await page.waitForSelector('#shipping')
        await page.waitForTimeout(5000)//等待5000毫秒
        const chooseAddress = '#checkout-step-shipping > div > div > div > div.shipping-address-item.not-selected-item > button.action.action-select-shipping-item'
        await page.waitForSelector(chooseAddress)
        await page.click(chooseAddress)//選擇地址簿
        await page.waitForSelector('#opc-sidebar > div.opc-block-summary')
        await page.waitForTimeout(10000)//等待10000毫秒
        //取得price, shipping, tax的文字
        const shippingCartSubtotalText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.sub > td > span')
        const shippingShippingText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.shipping.excl > td > span')
        const shippingTaxText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals-tax > td > span')
        const shippingOrderTotalText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.grand.totals > td > strong > span')
        //去除EUR 
        const shippingCartSubtotalNumIncludeComma = shippingCartSubtotalText.replace("EUR ","")
        const shippingShippingOnlyNumIncludeComma = shippingShippingText.replace("EUR ","")
        const shippingTaxOnlyNumIncludeComma = shippingTaxText.replace("EUR ","")
        const shippingOrderTotalNumIncludeComma = shippingOrderTotalText.replace("EUR ","")
        //去除逗號 
        function clear(str) { 
            str = str.replace(/,/g, "");//取消字串中出現的所有逗號 
            return str; 
        } 
        const shippingCartSubtotalNum = clear(shippingCartSubtotalNumIncludeComma)
        const shippingShippingOnlyNum = clear(shippingShippingOnlyNumIncludeComma)
        const shippingTaxOnlyNum = clear(shippingTaxOnlyNumIncludeComma)
        const shippingOrderTotalNum = clear(shippingOrderTotalNumIncludeComma)
        //將字串轉換成數字(包含小數值)
        const shippingCartSubtotal = parseFloat(shippingCartSubtotalNum)
        const shippingShipping = parseFloat(shippingShippingOnlyNum)
        const shippingTax = parseFloat(shippingTaxOnlyNum)
        const shippingOrderTotal = parseFloat(shippingOrderTotalNum )
        console.log('cartSubtotal(Shipping Page):',shippingCartSubtotal)
        console.log('shipping(Shipping Page):',shippingShipping)
        console.log('tax(Shipping Page):',shippingTax )
        console.log('orderTotal(Shipping Page):',shippingOrderTotal)
        //確認total information上的金額
        const shippingOrderTotalActual = shippingCartSubtotal+shippingShipping+shippingTax
        const shippingTaxRate = 0.2 //奧地利稅率
        const shippingCartSubtotalAndShipping = shippingCartSubtotal+shippingShipping
        const shippingTaxActualUnfixed = shippingCartSubtotalAndShipping*shippingTaxRate
        const shippingTaxActualFixed = shippingTaxActualUnfixed.toFixed(2)//四捨五入取到小數點第二位
        const shippingTaxActual= parseFloat(shippingTaxActualFixed)//因為toFixed(2)回傳的是字串
        console.log('totalActual(Shipping Page):',shippingOrderTotalActual)//實際計算(應得)的數字
        console.log('taxActual(Shipping Page):',shippingTaxActual)//實際計算(應得)的數字
        //確認total information和實際計算(應得)的數字是否相同
        expect(shippingOrderTotal).to.equal(shippingOrderTotalActual)
        expect(shippingTax).to.equal(shippingTaxActual)
        await click(page,'#next-step-trigger')//Next
        await page.waitForTimeout(20000)//等待20000毫秒
        await shouldNotExist(page,'#next-step-trigger')//Next
        await page.waitForTimeout(10000)//等待10000毫秒
    })
    it('Payment Page',async function(){
        const paymentPageUrl = await page.url()
        console.log('Payment Page URL:',paymentPageUrl)
        expect(paymentPageUrl).to.include('/checkout/#payment')//斷言:此page的url必須包含example.com
        await page.waitForSelector('#opc-sidebar > div.opc-block-summary')
        //取得price, shipping, tax的文字
        const paymentCartSubtotalText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.sub > td > span')
        const paymentShippingText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.shipping.excl > td > span')
        const paymentTaxText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals-tax > td > span')
        const paymentOrderTotalText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.grand.totals > td > strong > span')
        //去除EUR 
        const paymentCartSubtotalNumIncludeComma = paymentCartSubtotalText.replace("EUR ","")
        const paymentShippingOnlyNumIncludeComma = paymentShippingText.replace("EUR ","")
        const paymentTaxOnlyNumIncludeComma = paymentTaxText.replace("EUR ","")
        const paymentOrderTotalNumIncludeComma = paymentOrderTotalText.replace("EUR ","")
        //去除逗號 
        function clear(str) { 
            str = str.replace(/,/g, "");//取消字串中出現的所有逗號 
            return str; 
        } 
        const paymentCartSubtotalNum = clear(paymentCartSubtotalNumIncludeComma)
        const paymentShippingOnlyNum = clear(paymentShippingOnlyNumIncludeComma)
        const paymentTaxOnlyNum = clear(paymentTaxOnlyNumIncludeComma)
        const paymentOrderTotalNum = clear(paymentOrderTotalNumIncludeComma)
        //將字串轉換成數字
        const paymentCartSubtotal = parseFloat(paymentCartSubtotalNum)
        const paymentShipping = parseFloat(paymentShippingOnlyNum)
        const paymentTax = parseFloat(paymentTaxOnlyNum )
        const paymentOrderTotal = parseFloat(paymentOrderTotalNum )
        console.log('cartSubtotal(Payment Page):',paymentCartSubtotal)
        console.log('shipping(Payment Page):',paymentShipping)
        console.log('tax(Payment Page):',paymentTax )
        console.log('orderTotal(Payment Page):',paymentOrderTotal)
        //確認total information上的金額
        const paymentOrderTotalActual = paymentCartSubtotal+paymentShipping+paymentTax
        const paymentTaxRate = 0.2 //奧地利稅率
        const paymentCartSubtotalAndShipping = paymentCartSubtotal+paymentShipping
        const paymentTaxActualUnfixed = paymentCartSubtotalAndShipping*paymentTaxRate
        const paymentTaxActualFixed = paymentTaxActualUnfixed.toFixed(2)//四捨五入取到小數點第二位
        const paymentTaxActual= parseFloat(paymentTaxActualFixed)//因為toFixed(2)回傳的是字串
        console.log('totalActual(Payment Page):',paymentOrderTotalActual)//實際計算(應得)的數字
        console.log('taxActual(Payment Page):',paymentTaxActual)//實際計算(應得)的數字
        //確認total information和實際計算(應得)的數字是否相同
        expect(paymentOrderTotal).to.equal(paymentOrderTotalActual)
        expect(paymentTax).to.equal(paymentTaxActual)
        await page.waitForTimeout(5000)//等待5000毫秒
    })
    it('Cart Page - Remove Item',async function(){
        //移除購物車品項, 免得商品按到缺貨
        await page.goto('https://shop.benq.eu/eu-buy/checkout/cart/')
        await page.waitForSelector('#shopping-cart-table > tbody > tr > td.col.actionsiso > div > a.action.action-delete')
        await page.waitForTimeout(5000)//等待5000毫秒
        await page.click('#shopping-cart-table > tbody > tr > td.col.actionsiso > div > a.action.action-delete')
        await page.waitForTimeout(5000)//等待5000毫秒
        await page.waitForSelector('body > div.modals-wrapper > aside.modal-popup.confirm._show > div.modal-inner-wrap')
        await page.waitForSelector('body > div.modals-wrapper > aside.modal-popup.confirm._show > div.modal-inner-wrap > footer > button.action-primary.action-accept')
        await page.click('body > div.modals-wrapper > aside.modal-popup.confirm._show > div.modal-inner-wrap > footer > button.action-primary.action-accept')
        await page.waitForTimeout(5000)//等待5000毫秒
        await shouldNotExist(page,'body > div.modals-wrapper > aside.modal-popup.confirm._show > div.modal-inner-wrap > footer > button.action-primary.action-accept')
        await page.waitForSelector('#maincontent > div.columns > div > div.cart-empty > p:nth-child(1)')
        const emptyCart= await getText(page, '#maincontent > div.columns > div > div.cart-empty > p:nth-child(1)')
        expect(emptyCart).to.be.a('string','You have no items in your shopping cart.')
        await page.waitForTimeout(3000)//等待3000毫秒
        })
})

describe('BQE EU EC Order Process - Guest Checkout',()=>{
    let browser
    let page
    //Test Hooks:before, beforeEach, after, afterEach
    //before:每個test case執行之前先做的動作(page/browswer)
    before(async function(){
        browser=await puppeteer.launch({
            executablePath:
            "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            headless:true,//無介面模式:有無需要開視窗,false要開,true不開
            //slowMo:100,// slow down by 100ms
            //devtools:false//有無需要開啟開發人員工具
        })
        page=await browser.newPage()
        await page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
        await page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    })
    //after:每個test case執行之後統一要做的動作(page/browswer)
    after(async function(){
        await browser.close()
    })
    // beforeEach(async function(){
    //     //Runs before each test steps(pre-step;像是登入)
    //     await page.goto('http://example.com/')
    // })
    //afterEach(async function(){
        //Runs after each test steps(所有test step執行完的步驟)
    //})
    it('Go to Product Page',async function(){
        await page.setViewport({width:1200,height:1000})
        await page.goto('https://shop.benq.eu/eu-buy/ex3501r.html')
        const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
        await page.click(closeCookie)
        await page.waitForTimeout(5000)//等待5000毫秒
        const buyNow = '#product-addtocart-button'
        await page.click(buyNow)
        await page.waitForTimeout(20000)//等待20000毫秒
    })
    it('Cart Page',async function(){
        //Cart Page
        const cartPageUrl = await page.url()
        console.log('Cart Page URL:',cartPageUrl)
        expect(cartPageUrl).to.include('checkout/cart')//斷言:此page的url必須包含example.com
        const addItems = '#shopping-cart-table > tbody > tr > td.col.qty > div > div > div > button.increaseQty-cart-item'
        await page.click(addItems)//增加商品
        await page.waitForTimeout(5000)
        const qtyUpdate = '#shopping-cart-table > tbody > tr > td.col.qty > div > div > span.input-text-qty'
        await page.waitForSelector(qtyUpdate)//qty更新
        var qty = await getText(page, qtyUpdate)
        expect(qty).to.be.a('string','2')
        const guestCheckoutButton = '#maincontent > div.columns > div > div.cart-summary > ul > li:nth-child(3) > button'
        await page.waitForSelector(guestCheckoutButton)//Guest Checkout
        await page.click(guestCheckoutButton)//Guest Checkout
        await page.waitForTimeout(20000)//等待20000毫秒

    })
    it('Shipping Page',async function(){
        await page.waitForTimeout(5000)//等待10000毫秒
        await page.waitForSelector('#shipping')
        const shippingPageUrl = await page.url()
        console.log('Shipping Page URL:',shippingPageUrl)
        expect(shippingPageUrl).to.include('/checkout/')//斷言:此page的url必須包含example.com
        await page.waitForTimeout(5000)//等待5000毫秒
        await page.type('#customer-email','a4521005@outlook.com',{delay:10})//在email欄位輸入非會員email
        await page.waitForTimeout(5000)//等待100毫秒
        await page.type('#shipping-new-address-form > div:nth-child(1) > div > input','Celine',{delay:10})//在first name欄位輸入收件人first name
        await page.waitForTimeout(100)//等待100毫秒
        await page.type('#shipping-new-address-form > div:nth-child(2) > div > input','Chiu',{delay:10})//在last name欄位輸入收件人last name
        await page.waitForTimeout(100)//等待100毫秒
        const selectAustria = await page.$('select[name="country_id"]');
        await selectAustria.type('Austria');
        await page.waitForTimeout(5000)//等待5000毫秒
        await page.type('#shipping-new-address-form > fieldset > div > div > div > input','address',{delay:10})//在street address欄位輸入address
        await page.waitForTimeout(10000)//等待10000毫秒
        await page.keyboard.press('Enter')
        await page.type('#shipping-new-address-form > div:nth-child(8) > div > input','Eichgraben',{delay:10})//在city欄位輸入city
        await page.waitForTimeout(5000)//等待5000毫秒
        const selectBurgenland = await page.$('select[name="region_id"]');
        await selectBurgenland.type('Niederösterreich');
        await page.waitForTimeout(5000)//等待5000毫秒
        await page.type('#shipping-new-address-form > div:nth-child(11) > div > input','2413',{delay:10})//在zip code欄位輸入zip code
        await page.waitForTimeout(5000)//等待5000毫秒//奧地利會辨認郵遞區號所以要等久一點
        await page.type('#shipping-new-address-form > div:nth-child(12) > div > input','886929861005',{delay:10})//在Phone Number欄位輸入Phone Number
        await page.waitForTimeout(5000)//等待5000毫秒
        //取得price, shipping, tax的文字
        const shippingCartSubtotalText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.sub > td > span')
        const shippingShippingText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.shipping.excl > td > span')
        const shippingTaxText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals-tax > td > span')
        const shippingOrderTotalText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.grand.totals > td > strong > span')
        //去除EUR 
        const shippingCartSubtotalNumIncludeComma = shippingCartSubtotalText.replace("EUR ","")
        const shippingShippingOnlyNumIncludeComma = shippingShippingText.replace("EUR ","")
        const shippingTaxOnlyNumIncludeComma = shippingTaxText.replace("EUR ","")
        const shippingOrderTotalNumIncludeComma = shippingOrderTotalText.replace("EUR ","")
        //去除逗號 
        function clear(str) { 
            str = str.replace(/,/g, "");//取消字串中出現的所有逗號 
            return str; 
        } 
        const shippingCartSubtotalNum = clear(shippingCartSubtotalNumIncludeComma)
        const shippingShippingOnlyNum = clear(shippingShippingOnlyNumIncludeComma)
        const shippingTaxOnlyNum = clear(shippingTaxOnlyNumIncludeComma)
        const shippingOrderTotalNum = clear(shippingOrderTotalNumIncludeComma)
        //將字串轉換成數字(包含小數值)
        const shippingCartSubtotal = parseFloat(shippingCartSubtotalNum)
        const shippingShipping = parseFloat(shippingShippingOnlyNum)
        const shippingTax = parseFloat(shippingTaxOnlyNum)
        const shippingOrderTotal = parseFloat(shippingOrderTotalNum )
        console.log('cartSubtotal(Shipping Page):',shippingCartSubtotal)
        console.log('shipping(Shipping Page):',shippingShipping)
        console.log('tax(Shipping Page):',shippingTax )
        console.log('orderTotal(Shipping Page):',shippingOrderTotal)
        //確認total information上的金額
        const shippingOrderTotalActual = shippingCartSubtotal+shippingShipping+shippingTax
        const shippingTaxRate = 0.2 //奧地利稅率
        const shippingCartSubtotalAndShipping = shippingCartSubtotal+shippingShipping
        const shippingTaxActualUnfixed = shippingCartSubtotalAndShipping*shippingTaxRate
        const shippingTaxActualFixed = shippingTaxActualUnfixed.toFixed(2)//四捨五入取到小數點第二位
        const shippingTaxActual= parseFloat(shippingTaxActualFixed)//因為toFixed(2)回傳的是字串
        console.log('totalActual(Shipping Page):',shippingOrderTotalActual)//實際計算(應得)的數字
        console.log('taxActual(Shipping Page):',shippingTaxActual)//實際計算(應得)的數字
        //確認total information和實際計算(應得)的數字是否相同
        expect(shippingOrderTotal).to.equal(shippingOrderTotalActual)
        expect(shippingTax).to.equal(shippingTaxActual)
        await click(page,'#next-step-trigger')//Next
        await shouldNotExist(page,'#next-step-trigger')//Next

    })

    it('Payment Page',async function(){
        await page.waitForTimeout(5000)//等待5000毫秒
        await page.waitForSelector('#co-payment-form > fieldset > legend > span')
        await page.waitForSelector('#co-payment-form > fieldset > div.shipping-information')
        const paymentPageUrl = await page.url()
        console.log('Payment Page URL:',paymentPageUrl)
        expect(paymentPageUrl).to.include('/checkout/#payment')//斷言:此page的url必須包含example.com
        await page.waitForTimeout(5000)//等待5000毫秒
        await page.waitForSelector('#opc-sidebar > div.opc-block-summary')
        //取得price, shipping, tax的文字
        const paymentCartSubtotalText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.sub > td > span')
        const paymentShippingText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.shipping.excl > td > span')
        const paymentTaxText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals-tax > td > span')
        const paymentOrderTotalText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.grand.totals > td > strong > span')
        //去除EUR 
        const paymentCartSubtotalNumIncludeComma = paymentCartSubtotalText.replace("EUR ","")
        const paymentShippingOnlyNumIncludeComma = paymentShippingText.replace("EUR ","")
        const paymentTaxOnlyNumIncludeComma = paymentTaxText.replace("EUR ","")
        const paymentOrderTotalNumIncludeComma = paymentOrderTotalText.replace("EUR ","")
        //去除逗號 
        function clear(str) { 
            str = str.replace(/,/g, "");//取消字串中出現的所有逗號 
            return str; 
        } 
        const paymentCartSubtotalNum = clear(paymentCartSubtotalNumIncludeComma)
        const paymentShippingOnlyNum = clear(paymentShippingOnlyNumIncludeComma)
        const paymentTaxOnlyNum = clear(paymentTaxOnlyNumIncludeComma)
        const paymentOrderTotalNum = clear(paymentOrderTotalNumIncludeComma)
        //將字串轉換成數字
        const paymentCartSubtotal = parseFloat(paymentCartSubtotalNum)
        const paymentShipping = parseFloat(paymentShippingOnlyNum)
        const paymentTax = parseFloat(paymentTaxOnlyNum )
        const paymentOrderTotal = parseFloat(paymentOrderTotalNum )
        console.log('cartSubtotal(Payment Page):',paymentCartSubtotal)
        console.log('shipping(Payment Page):',paymentShipping)
        console.log('tax(Payment Page):',paymentTax )
        console.log('orderTotal(Payment Page):',paymentOrderTotal)
        //確認total information上的金額
        const paymentOrderTotalActual = paymentCartSubtotal+paymentShipping+paymentTax
        const paymentTaxRate = 0.2 //奧地利稅率
        const paymentCartSubtotalAndShipping = paymentCartSubtotal+paymentShipping
        const paymentTaxActualUnfixed = paymentCartSubtotalAndShipping*paymentTaxRate
        const paymentTaxActualFixed = paymentTaxActualUnfixed.toFixed(2)//四捨五入取到小數點第二位
        const paymentTaxActual= parseFloat(paymentTaxActualFixed)//因為toFixed(2)回傳的是字串
        console.log('totalActual(Payment Page):',paymentOrderTotalActual)//實際計算(應得)的數字
        console.log('taxActual(Payment Page):',paymentTaxActual)//實際計算(應得)的數字
        //確認total information和實際計算(應得)的數字是否相同
        expect(paymentOrderTotal).to.equal(paymentOrderTotalActual)
        expect(paymentTax).to.equal(paymentTaxActual)
        await page.waitForTimeout(5000)//等待5000毫秒
        })
})
