const { Given, When, Then, Before, After } = require('cucumber')
const { expect } = require('chai')
const puppeteer = require('puppeteer')

Before({timeout: 4 * 5000},async function () {
    this.browser = await puppeteer.launch({ 
        executablePath:
            "../../.local-chromium/win32-901912/chrome-win/chrome",
        headless: false,
        slowMo:100,// slow down by 100ms
        devtools:false//有無需要開啟開發人員工具
    })
    this.page = await this.browser.newPage()
    await this.page.setViewport({width:1200,height:1000})
    await this.page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
    await this.page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
})

After({timeout: 4 * 5000},async function () {
    await this.browser.close()
})

Given("Go to All monitor Page - Check number",{timeout: 12 * 5000},async function(){
    await this.page.goto('https://www.benq.com/en-us/monitor.html')
    //要點六次+
    await this.page.click('#seriesproducts > div.right > div.more > i')
    await this.page.waitForTimeout(5000)//等待5000毫秒
    await this.page.click('#seriesproducts > div.right > div.more > i')
    await this.page.waitForTimeout(5000)//等待5000毫秒
    await this.page.click('#seriesproducts > div.right > div.more > i')
    await this.page.waitForTimeout(5000)//等待5000毫秒
    await this.page.click('#seriesproducts > div.right > div.more > i')
    await this.page.waitForTimeout(5000)//等待5000毫秒
    await this.page.click('#seriesproducts > div.right > div.more > i')
    await this.page.waitForTimeout(5000)//等待5000毫秒
    await this.page.click('#seriesproducts > div.right > div.more > i')
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const allMontior = " #seriesproducts > div.right > ul.products > ul > li"
    //step2:計算每一個Series Page數量(ex:假設現在是10個, 之後數量如果增加了, 腳本就要改) 
    const countallMontior = await this.page.$$eval(allMontior,element=>element.length)
    console.log("Total of All Montior:",countallMontior)
    expect(countallMontior).to.equal(54)//目前Monitor有54個產品
})

When("check each Spec exist or not-1",{timeout: 4 * 5000},async function(){
    //step3:取得Series Page上的每一個Product Card 的URL
    await this.page.goto('https://www.benq.com/en-us/monitor.html')
    //第一個projector
    var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(1) > a"
    var allMontiorProductURL = await this.page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
    // console.log(gamingProjectorProductURL)
    //step4:去除.html然後加上/specifications.html
    var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
    //console.log(gamingProjectorURL)
    var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
    console.log(allMontiorSpecURL)
    //step5:改好URL以後, 在各自前往此page
    await this.page.goto(allMontiorSpecURL)
    //step6:檢查spec是否空白(是否存在".block")
    await this. page.waitForSelector('.block', {visible:true})
})

When("check each Spec exist or not-2",{timeout: 4 * 5000},async function(){
    //step3:取得Series Page上的每一個Product Card 的URL
    await this.page.goto('https://www.benq.com/en-us/monitor.html')
    //第一個projector
    var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(2) > a"
    var allMontiorProductURL = await this.page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
    // console.log(gamingProjectorProductURL)
    //step4:去除.html然後加上/specifications.html
    var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
    //console.log(gamingProjectorURL)
    var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
    console.log(allMontiorSpecURL)
    //step5:改好URL以後, 在各自前往此page
    await this.page.goto(allMontiorSpecURL)
    //step6:檢查spec是否空白(是否存在".block")
    await this. page.waitForSelector('.block', {visible:true})
    
})

Then("check each Spec exist or not-3",{timeout: 4 * 5000},async function(){
    //step3:取得Series Page上的每一個Product Card 的URL
    await this.page.goto('https://www.benq.com/en-us/monitor.html')
    //第一個projector
    var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(3) > a"
    var allMontiorProductURL = await this.page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
    // console.log(gamingProjectorProductURL)
    //step4:去除.html然後加上/specifications.html
    var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
    //console.log(gamingProjectorURL)
    var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
    console.log(allMontiorSpecURL)
    //step5:改好URL以後, 在各自前往此page
    await this.page.goto(allMontiorSpecURL)
    //step6:檢查spec是否空白(是否存在".block")
    await this. page.waitForSelector('.block', {visible:true})
    
})