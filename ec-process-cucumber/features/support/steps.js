const { Given, When, Then, Before, After } = require('@cucumber/cucumber')
const { expect } = require('chai')
const puppeteer = require('puppeteer')

Before({timeout: 24 * 5000},async function () {
    this.browser = await puppeteer.launch({ 
        executablePath:
        "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
        headless:true,//有無需要開視窗,false要開,true不開
        slowMo:100,// slow down by 100ms
        devtools:false//有無需要開啟開發人員工具
    })
    this.page = await this.browser.newPage()
    await this.page.setViewport({width:1200,height:1000})
    await this.page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
    await this.page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
})

After({timeout: 12 * 5000},async function () {
    await this.browser.close()
})

Given("[EN-EU Guest]Go to Product Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://shop.benq.eu/eu-buy/ex3501r.html')
    const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const buyNow = '#product-addtocart-button'
    await this.page.waitForSelector(buyNow)
    await this.page.click(buyNow)
    await this.page.waitForTimeout(20000)//等待20000毫秒
})

When("[EN-EU Guest]Cart Page",{timeout: 12 * 5000},async function(){
    //Cart Page
    const cartPageUrl = await this.page.url()
    console.log('Cart Page URL:',cartPageUrl)
    expect(cartPageUrl).to.include('checkout/cart')//斷言:此page的url必須包含example.com
    const addItems = '#shopping-cart-table > tbody > tr > td.col.qty > div > div > div > button.increaseQty-cart-item'
    await this.page.waitForSelector(addItems)
    await this.page.click(addItems)//增加商品
    await this.page.waitForTimeout(5000)
    const qtyUpdate = '#shopping-cart-table > tbody > tr > td.col.qty > div > div > span.input-text-qty'
    await this.page.waitForSelector(qtyUpdate)//qty更新
    var qty = await this.page.$eval(qtyUpdate,element=>element.innerHTML)
    expect(qty).to.be.a('string','2')
    const guestCheckoutButton = '#maincontent > div.columns > div > div.cart-summary > ul > li:nth-child(3) > button'
    await this.page.waitForSelector(guestCheckoutButton)//Guest Checkout
    await this.page.click(guestCheckoutButton)//Guest Checkout
    await this.page.waitForTimeout(20000)//等待20000毫秒
})

When("[EN-EU Guest]Shipping Page",{timeout: 24 * 5000},async function(){
    await this.page.waitForTimeout(5000)//等待10000毫秒
    await this.page.waitForSelector('#shipping')
    const shippingPageUrl = await this.page.url()
    console.log('Shipping Page URL:',shippingPageUrl)
    expect(shippingPageUrl).to.include('/checkout/')//斷言:此page的url必須包含example.com
    await this.page.waitForTimeout(5000)//等待5000毫秒
    await this.page.type('#customer-email','a4521005@outlook.com',{delay:10})//在email欄位輸入非會員email
    await this.page.waitForTimeout(5000)//等待100毫秒
    await this.page.type('#shipping-new-address-form > div:nth-child(1) > div > input','Celine',{delay:10})//在first name欄位輸入收件人first name
    await this.page.waitForTimeout(100)//等待100毫秒
    await this.page.type('#shipping-new-address-form > div:nth-child(2) > div > input','Chiu',{delay:10})//在last name欄位輸入收件人last name
    await this.page.waitForTimeout(100)//等待100毫秒
    const selectAustria = await this.page.$('select[name="country_id"]');
    await selectAustria.type('Austria');
    await this.page.waitForTimeout(5000)//等待5000毫秒
    await this.page.type('#shipping-new-address-form > fieldset > div > div > div > input','address',{delay:10})//在street address欄位輸入address
    await this.page.waitForTimeout(10000)//等待10000毫秒
    await this.page.keyboard.press('Enter')
    await this.page.type('#shipping-new-address-form > div:nth-child(8) > div > input','Eichgraben',{delay:10})//在city欄位輸入city
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const selectBurgenland = await this.page.$('select[name="region_id"]');
    await selectBurgenland.type('Niederösterreich');
    await this.page.waitForTimeout(5000)//等待5000毫秒
    await this.page.type('#shipping-new-address-form > div:nth-child(11) > div > input','2413',{delay:10})//在zip code欄位輸入zip code
    await this.page.waitForTimeout(5000)//等待5000毫秒//奧地利會辨認郵遞區號所以要等久一點
    await this.page.type('#shipping-new-address-form > div:nth-child(12) > div > input','886929861005',{delay:10})//在Phone Number欄位輸入Phone Number
    await this.page.waitForTimeout(5000)//等待5000毫秒
    //取得price, shipping, tax的文字
    const shippingCartSubtotalTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.sub > td > span'
    await this.page.waitForSelector(shippingCartSubtotalTextElement)
    const shippingCartSubtotalText = await this.page.$eval(shippingCartSubtotalTextElement,element=>element.innerHTML)
    //const shippingCartSubtotalText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.sub > td > span')
    const shippingShippingTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.shipping.excl > td > span'
    await this.page.waitForSelector(shippingShippingTextElement)
    const shippingShippingText = await this.page.$eval(shippingShippingTextElement,element=>element.innerHTML)
    //const shippingShippingText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.shipping.excl > td > span')
    const shippingTaxTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals-tax > td > span'
    await this.page.waitForSelector(shippingTaxTextElement)
    const shippingTaxText = await this.page.$eval(shippingTaxTextElement,element=>element.innerHTML)
    //const shippingTaxText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals-tax > td > span')
    const shippingOrderTotalElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.grand.totals > td > strong > span'
    await this.page.waitForSelector(shippingOrderTotalElement)
    const shippingOrderTotalText = await this.page.$eval(shippingOrderTotalElement,element=>element.innerHTML)
    //const shippingOrderTotalText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.grand.totals > td > strong > span')
        
    //去除EUR 
    const shippingCartSubtotalNumIncludeComma = shippingCartSubtotalText.replace("EUR ","")
    const shippingShippingOnlyNumIncludeComma = shippingShippingText.replace("EUR ","")
    const shippingTaxOnlyNumIncludeComma = shippingTaxText.replace("EUR ","")
    const shippingOrderTotalNumIncludeComma = shippingOrderTotalText.replace("EUR ","")
    //去除逗號 
    function clear(str) { 
        str = str.replace(/,/g, "");//取消字串中出現的所有逗號 
        return str; 
    } 
    const shippingCartSubtotalNum = clear(shippingCartSubtotalNumIncludeComma)
    const shippingShippingOnlyNum = clear(shippingShippingOnlyNumIncludeComma)
    const shippingTaxOnlyNum = clear(shippingTaxOnlyNumIncludeComma)
    const shippingOrderTotalNum = clear(shippingOrderTotalNumIncludeComma)
    //將字串轉換成數字(包含小數值)
    const shippingCartSubtotal = parseFloat(shippingCartSubtotalNum)
    const shippingShipping = parseFloat(shippingShippingOnlyNum)
    const shippingTax = parseFloat(shippingTaxOnlyNum)
    const shippingOrderTotal = parseFloat(shippingOrderTotalNum )
    console.log('cartSubtotal(Shipping Page):',shippingCartSubtotal)
    console.log('shipping(Shipping Page):',shippingShipping)
    console.log('tax(Shipping Page):',shippingTax )
    console.log('orderTotal(Shipping Page):',shippingOrderTotal)
    //確認total information上的金額
    const shippingOrderTotalActual = shippingCartSubtotal+shippingShipping+shippingTax
    const shippingTaxRate = 0.2 //奧地利稅率
    const shippingCartSubtotalAndShipping = shippingCartSubtotal+shippingShipping
    const shippingTaxActualUnfixed = shippingCartSubtotalAndShipping*shippingTaxRate
    const shippingTaxActualFixed = shippingTaxActualUnfixed.toFixed(2)//四捨五入取到小數點第二位
    const shippingTaxActual= parseFloat(shippingTaxActualFixed)//因為toFixed(2)回傳的是字串
    console.log('totalActual(Shipping Page):',shippingOrderTotalActual)//實際計算(應得)的數字
    console.log('taxActual(Shipping Page):',shippingTaxActual)//實際計算(應得)的數字
    //確認total information和實際計算(應得)的數字是否相同
    expect(shippingOrderTotal).to.equal(shippingOrderTotalActual)
    expect(shippingTax).to.equal(shippingTaxActual)
    await this.page.click('#next-step-trigger')//Next
    await this.page.waitForSelector('#next-step-trigger',{
        hidden:true, 
        timeout:10000
    })//等待element隱藏的時間是否在10000毫秒內
    //await shouldNotExist(page,'#next-step-trigger')//Next
})

Then("[EN-EU Guest]Payment Page",{timeout: 12 * 5000},async function(){
    await this.page.waitForTimeout(5000)//等待5000毫秒
    await this.page.waitForSelector('#co-payment-form > fieldset > legend > span')
    await this.page.waitForSelector('#co-payment-form > fieldset > div.shipping-information')
    const paymentPageUrl = await this.page.url()
    console.log('Payment Page URL:',paymentPageUrl)
    expect(paymentPageUrl).to.include('/checkout/#payment')//斷言:此page的url必須包含example.com
    await this.page.waitForTimeout(5000)//等待5000毫秒
    await this.page.waitForSelector('#opc-sidebar > div.opc-block-summary')
    //取得price, shipping, tax的文字
    const paymentCartSubtotalTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.sub > td > span'
    await this.page.waitForSelector(paymentCartSubtotalTextElement)
    const paymentCartSubtotalText = await this.page.$eval(paymentCartSubtotalTextElement,element=>element.innerHTML)
    //const paymentCartSubtotalText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.sub > td > span')
    const paymentShippingTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.shipping.excl > td > span'
    await this.page.waitForSelector(paymentShippingTextElement)
    const paymentShippingText = await this.page.$eval(paymentShippingTextElement,element=>element.innerHTML)    
    //const paymentShippingText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.shipping.excl > td > span')
    const paymentTaxTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals-tax > td > span'
    await this.page.waitForSelector(paymentTaxTextElement)
    const paymentTaxText = await this.page.$eval(paymentTaxTextElement,element=>element.innerHTML)    
    //const paymentTaxText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals-tax > td > span')
    const paymentOrderTotalTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.grand.totals > td > strong > span'
    await this.page.waitForSelector(paymentOrderTotalTextElement)
    const paymentOrderTotalText = await this.page.$eval(paymentOrderTotalTextElement,element=>element.innerHTML)
    //const paymentOrderTotalText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.grand.totals > td > strong > span')
    //去除EUR 
    const paymentCartSubtotalNumIncludeComma = paymentCartSubtotalText.replace("EUR ","")
    const paymentShippingOnlyNumIncludeComma = paymentShippingText.replace("EUR ","")
    const paymentTaxOnlyNumIncludeComma = paymentTaxText.replace("EUR ","")
    const paymentOrderTotalNumIncludeComma = paymentOrderTotalText.replace("EUR ","")
    //去除逗號 
    function clear(str) { 
        str = str.replace(/,/g, "");//取消字串中出現的所有逗號 
        return str; 
    } 
    const paymentCartSubtotalNum = clear(paymentCartSubtotalNumIncludeComma)
    const paymentShippingOnlyNum = clear(paymentShippingOnlyNumIncludeComma)
    const paymentTaxOnlyNum = clear(paymentTaxOnlyNumIncludeComma)
    const paymentOrderTotalNum = clear(paymentOrderTotalNumIncludeComma)
    //將字串轉換成數字
    const paymentCartSubtotal = parseFloat(paymentCartSubtotalNum)
    const paymentShipping = parseFloat(paymentShippingOnlyNum)
    const paymentTax = parseFloat(paymentTaxOnlyNum )
    const paymentOrderTotal = parseFloat(paymentOrderTotalNum )
    console.log('cartSubtotal(Payment Page):',paymentCartSubtotal)
    console.log('shipping(Payment Page):',paymentShipping)
    console.log('tax(Payment Page):',paymentTax )
    console.log('orderTotal(Payment Page):',paymentOrderTotal)
    //確認total information上的金額
    const paymentOrderTotalActual = paymentCartSubtotal+paymentShipping+paymentTax
    const paymentTaxRate = 0.2 //奧地利稅率
    const paymentCartSubtotalAndShipping = paymentCartSubtotal+paymentShipping
    const paymentTaxActualUnfixed = paymentCartSubtotalAndShipping*paymentTaxRate
    const paymentTaxActualFixed = paymentTaxActualUnfixed.toFixed(2)//四捨五入取到小數點第二位
    const paymentTaxActual= parseFloat(paymentTaxActualFixed)//因為toFixed(2)回傳的是字串
    console.log('totalActual(Payment Page):',paymentOrderTotalActual)//實際計算(應得)的數字
    console.log('taxActual(Payment Page):',paymentTaxActual)//實際計算(應得)的數字
    //確認total information和實際計算(應得)的數字是否相同
    expect(paymentOrderTotal).to.equal(paymentOrderTotalActual)
    expect(paymentTax).to.equal(paymentTaxActual)
    await this.page.waitForTimeout(5000)//等待5000毫秒
    
})