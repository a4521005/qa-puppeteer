Feature:EC Process
    tests:
    1. users can checkout successfully
    2. amount of money on order summary is correct
    Scenario: BQE EU EC Order Process - Guest Checkout
        Given [EN-EU Guest]Go to Product Page
        When [EN-EU Guest]Click Buy Now Button 
        Then [EN-EU Guest]add items successfully
        And [EN-EU Guest]Navigatie to Cart Page Successfully 
        When [EN-EU Guest]update qty on Cart Page 
        Then [EN-EU Guest]update qty on Cart Page Successfully
        When [EN-EU Guest]click Guest Checkout button
        Then [EN-EU Guest]check out successfully
        And [EN-EU Guest]Navigatie to shipping Page Successfully 
        When [EN-EU Guest]input shipping information
        When [EN-EU Guest]calculate all amounts of money in order summary on shipping page 
        Then [EN-EU Guest]all amounts of money in order summary on shipping page are correct
        When [EN-EU Guest]click next
        Then [EN-EU Guest]Navigatie to Payment Page successfully
        When [EN-EU Guest]calculate all amounts of money in order summary on payment page 
        Then [EN-EU Guest]all amounts of money in order summary on payment page  are correct
    Scenario: BQE EU EC Order Process - Member Checkout
        Given [EN-EU Member]Go to Product Page
        When [EN-EU Member]Cart Page
        And [EN-EU Member]Login Page
        And [EN-EU Member]Shipping Page
        And [EN-EU Member]Payment Page
        Then [EN-EU Member]Cart Page - Remove Item


    
        
